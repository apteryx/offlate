This file summarizes changes between releases.

Changes since 0.5
-----------------

### Improvements and features ###

* First-time users now get a specific window to help them with initial configuration.
* Fixed TS format not setting the obsolete flag when saving.
* Whitespace characters are shown with a small symbol in the editor. Spaces get
  a small middle dot, no-break spaces get a line underneath. Tabs are also represented.
* Some improvements have been made to the code documentation.
* The internals have been refactored. The most visible difference is in
  settings handling: systems define the settings they need, instead of having
  this information spread everywhere.
* When a crash happens, suggest sending a bug report and show traceback. Obviously,
  it would be much better to fix the actual crashes :-)

### Bug fixes ###

* Fix version before sending to the TP.
* Fix crash when clicking on a tag to copy it.
* Fix XML injection in strings: if translations contain XML tags, they are no
  longer interpreted as formating and are instead shown in full.

### Languages ###

Changes in 0.5
--------------

### Improvements and features ###

* A binary bundle for running the application under Linux and Windows is now
  available. Users do not need to compile and install dependencies by themselves,
  although it is still recommended.
* New button to open a project in an external editor. When in the external editor,
  offlate's edition zone is grayed out but kept in sync with what the external
  editor is doing. When the external editor is closed, offlate will again allow
  edition.
* New format for Android strings.xml files.
* New format for Android appstore format (description for Fdroid and other stores
  in the `fastlane` directory).
* New Github system. You can now contribute to projects that are hosted on that
  platform.

### UI Changes ###

* No more freeze when uploading or updating a project: it is done on a separate
  thread.
* Double-click on a project on the welcome screen opens it in the editor.

### Bug fixes ###

* Fix crashes caused by an empty initial configuration, especially at first run,
  in the settings window and the new project window.
* Fix errors when showing error messages, caused by missing imports.
* Fix crash when opening settings window from the editor.
* Add missing dependencies to setup.py preventing pip install offlate from working.

### Languages ###

Changes in 0.4
--------------

### Improvements and features ###

* When an error occurs while fetching a project, an error message is shown and
  the new project window re-opens with the same details as before
* Create a new ts file for languages not yet supported by a Qt project
* Open the settings window when creating a project for which the platform was
  not configured yet

### UI Changes ###

* Fix tabulation order when creating and editing project settings
* Add missing icon to the build result

### Bug fixes ###

* Correctly pass application version to the gettext format
* When editing a project, system specific fields were not shown
* Unsupported formats raises a specific exception that can be caught by the UI
* Stop crashing when an error occurs during project fetching
* Properly load empty translations in ts format
* Properly update fuzzy flags in ts format
* Fix crash when editing a project
* Fix fetching a non default branch of a git project

### Languages ###

* Update 'en' and 'fr' translations.

Changes in 0.3
--------------

* Qt translation files (.ts) support
* Gitlab support
** Multiple instances can be set up in settings
** Sends merge requests upstream
* Project management window
** Acts as a welcoming screen
** Allows to easily create, open, edit and delete projects
** Better access to settings and credits
* Creating and loading a new project will not freeze the app anymore
** Feedback is given with a progress bar in the status bar
** Loading of a new project is done in a separate worker thread
