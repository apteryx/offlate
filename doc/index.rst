.. Offlate documentation master file, created by
   sphinx-quickstart on Sat Feb 17 19:48:43 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Offlate's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   format
   entry
   project

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
