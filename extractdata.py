from lxml import html
import requests
import json

data = []

print("Analysing projects at the TP...")
tplist = requests.get("https://translationproject.org/domain/index.html")
if(tplist.status_code == 200):
    tree = html.fromstring(tplist.content)
    domains = tree.xpath('//table/tr/td[1]/a/text()')
    for d in domains:
        data.append({"name": d, "system": 0})

print("Analysing projects weblate instances...")
weblateservers = ['https://hosted.weblate.org', 'https://translate.fedoraproject.org']
for server in weblateservers:
    print("server: {}".format(server))
    url = server + '/api/projects/'
    while url is not None:
        lst = requests.get(url)
        res = json.loads(lst.content)
        url = res['next']
        res = res['results']
        for r in res:
            data.append({'name': r['name'], 'system': 4, 'instance': server,
                'project': r['slug']})

print("Analysing projects at transifex")
transifexlist = {
    'openstreetmap': ['id-editor', 'osmybiz', 'openinghoursfragment',
        'presets', 'vespucci'],
}
for k in transifexlist:
    for proj in transifexlist[k]:
        data.append({'name': proj, 'system': 1, 'organisation': k})

data.append({"name": "offlate", "system": 2,
    "repo": "https://framagit.org/tyreunom/offlate", "branch": "master"})

print("done, writing results")
with open('offlate/data.json', 'w') as f:
    f.write(json.dumps(data))
