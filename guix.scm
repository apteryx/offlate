(use-modules
  ((guix licenses) #:prefix license:)
  (guix build-system python)
  (guix download)
  (guix git-download)
  (guix git)
  (guix packages)
  (guix utils)
  (gnu packages aspell)
  (gnu packages check)
  (gnu packages enchant)
  (gnu packages fontutils)
  (gnu packages freedesktop)
  (gnu packages libreoffice)
  (gnu packages python)
  (gnu packages python-web)
  (gnu packages python-xyz)
  (gnu packages qt)
  (gnu packages serialization)
  (gnu packages sphinx)
  (gnu packages time)
  (gnu packages version-control)
  (gnu packages xml))

(define my-python-android-stringslib
  (package
    (inherit python-android-stringslib)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://framagit.org/tyreunom/python-android-strings-lib")
                     (commit "bea536ba7878ac1b569af45bffa2e4396bf134dc")))
              (file-name (git-file-name "python-android-stringslib" "1.2"))
              (sha256
               (base32
                "01adzm17cs5980f7d2222lpgdar5n99chyap7541aa4wc6gm9wqs"))))))

(define python-lxml-4.6
  (package
    (inherit python-lxml)
    (version "4.6.4")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "lxml" version))
        (sha256
          (base32 "171rs6w8rj5nfg3zzcwgllz6i3csw1ci0glbjajwgw9ixqgvvyfs"))))))

(define-public offlate
  (package
    (name "offlate")
    (version "0.5")
    (source (git-checkout (url (dirname (current-filename)))))
    (build-system python-build-system)
    (arguments
     `(#:modules ((guix build python-build-system)
                  (guix build qt-utils)
                  (guix build utils))
       #:imported-modules (,@%python-build-system-modules
                            (guix build qt-utils))
       ;; No tests
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'generate-fonts
           (lambda _
             (invoke "make" "fonts")))
         (add-before 'build 'generate-translations
           (lambda _
             (invoke "make" "update-langs")))
         (add-after 'install 'wrap-qt
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (wrap-qt-program "offlate" #:output out #:inputs inputs))))
         (add-after 'install 'wrap-offlate
            (lambda* (#:key outputs #:allow-other-keys)
              (wrap-program
                  (string-append (assoc-ref outputs "out") "/bin/offlate")
                `("PYTHONPATH" ":" prefix
                  (,(string-append (getenv "PYTHONPATH") ":"
                   (assoc-ref outputs "out") "/lib/python"
                   ,(version-major+minor (package-version python-wrapper))
                   "/site-packages")))))))))
    (inputs
      `(("python-android-stringslib" ,my-python-android-stringslib)
        ("python-dateutil" ,python-dateutil)
        ("python-gitlab" ,python-gitlab)
        ("python-lxml" ,python-lxml-4.6)
        ("python-polib" ,python-polib)
        ("python-pycountry" ,python-pycountry)
        ("python-pyenchant" ,python-pyenchant)
        ("python-pygit2" ,python-pygit2)
        ("python-pygithub" ,python-pygithub)
        ("python-pyqt" ,python-pyqt)
        ("python-requests" ,python-requests)
        ("python-ruamel.yaml" ,python-ruamel.yaml)
        ("python-translate-toolkit" ,python-translate-toolkit)
        ("python-translation-finder" ,python-translation-finder)
        ("python-watchdog" ,python-watchdog)))
    (native-inputs
     `(("fontforge" ,fontforge)
       ("qttools" ,qttools)
       ("python-sphinx" ,python-sphinx)))
    (home-page "https://framagit.org/tyreunom/offlate")
    (synopsis "Offline translation interface for online translation tools")
    (description "Offlate offers a unified interface for different translation
file formats, as well as many different online translation platforms.  You can
use it to get work from online platforms, specialized such as the Translation
Project, or not such a gitlab instance when your upstream doesn't use any
dedicated platform.  The tool proposes a unified interface for any format and
an upload option to send your work back to the platform.")
    (license license:gpl3+)))

offlate
