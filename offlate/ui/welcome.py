#   Copyright (c) 2021 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys
import os

from .new import PredefinedProjectWidget, AdvancedProjectWidget
from .manager import NewRunnable
from .config.settings import SystemSettingsWidget, CopyrightSettingsWidget, \
    LangSettingsWidget

WelcomePage = 1
CopyrightPage = 2
LangPage = 3
ProjectPage = 4
DetailPage = 5
SettingsPage = 6
DownloadPage = 7
SuccessPage = 8
HintsPage = 9

class WelcomeWindow(QWizard):
    """
    This it the class that contains the first-time welcome screen, implemented
    as a wizard, to benefit from the page logic of the class. It is mostly
    linera, but we add one optional page to add the details of a project, if
    the first project is not available in the list.
    """
    def __init__(self, manager):
        super().__init__()
        self.manager = manager
        self.initUI()

    def initUI(self):
        center = QDesktopWidget().availableGeometry().center()
        self.addPage(PageWelcome(self))
        self.addPage(PageCopyright(self))
        self.addPage(PageLang(self))
        self.addPage(PageProjectSelection(self))
        self.addPage(PageSystemSettings(self))
        self.addPage(PageDownload(self))
        self.addPage(PageSuccess(self))
        self.addPage(PageHints(self))
        self.setGeometry(center.x()-300, center.y()-225, 600, 450)
        self.setWindowTitle(self.tr('Welcome to Offlate'))
        self.setWizardStyle(QWizard.ModernStyle);
        self.setPixmap(QWizard.LogoPixmap, QPixmap(os.path.dirname(__file__) + '/data/icon.png'));


class PageWelcome(QWizardPage):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        self.setTitle("Offlate")
        self.setPixmap(QWizard.WatermarkPixmap, QPixmap(os.path.dirname(__file__) + '/data/illustration.png'))

        #self.setSubTitle(self.tr("Welcome to Offlate!"))

        explainText = QLabel(self)
        explainText.setText(self.tr("Offlate is a tool to help you localise \
free and open source software. Before you start contributing translations to a \
project though, there are a few things we need to set up and talk about. Let's \
get started!"))
        explainText.setWordWrap(True)
        layout.addWidget(explainText)
        
        self.setLayout(layout)

class PageCopyright(QWizardPage):
    def __init__(self, parent):
        super().__init__(parent)
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        self.setTitle(self.tr("Copyright Settings"))
        self.setSubTitle(self.tr("Configuring how your copyright is added to files"))

        self.copyright = CopyrightSettingsWidget()
        layout.addWidget(self.copyright)

        self.setLayout(layout)

        self.registerField("name*", self.copyright.nameWidget)
        self.registerField("email*", self.copyright.emailWidget)

class PageLang(QWizardPage):
    def __init__(self, parent):
        super().__init__(parent)
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        self.setTitle(self.tr("Language Settings"))
        self.setSubTitle(self.tr("Configuring the default language for new projects"))

        self.lang = LangSettingsWidget()
        layout.addWidget(self.lang)
        self.lang.changed.connect(self.langChanged)

        self.setLayout(layout)

        self.langEdit = QLineEdit()
        self.registerField("lang*", self.langEdit)

    def langChanged(self, lang):
        self.langEdit.setText(lang)

class PageProjectSelection(QWizardPage):
    def __init__(self, parent):
        super().__init__(parent)
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        self.setTitle(self.tr("First Project"))
        self.setSubTitle(self.tr("Choosing a first project"))

        self.predefinedprojects = PredefinedProjectWidget()
        self.advancedproject = AdvancedProjectWidget()
        self.predefinedprojects.currentItemChanged.connect(self.selected)
        layout.addWidget(self.predefinedprojects)

        self.setLayout(layout)

        self.registerField("project*", self.advancedproject, "data",
                self.advancedproject.modified)

    def selected(self):
        self.advancedproject.fill(self.predefinedprojects.currentData())

class PageSystemSettings(QWizardPage):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.initUI()

    def initUI(self):
        self.setTitle(self.tr("System Configuration"))
        self.setSubTitle(self.tr("Getting an account on the project's platform"))

    def initializePage(self):
        layout = QVBoxLayout()
        system = self.field("project")["system"]
        info = self.field("project")["info"]
        lang = self.field("lang")
        email = self.field("email")
        name = self.field("name")
        self.parent.manager.updateSettings({'Generic': {'name': name, 'email': email, 'lang': lang}})
        widget = SystemSettingsWidget(self.parent.manager, system = system, parent = self, data = info)
        layout.addWidget(widget)
        self.setLayout(layout)


class PageDownload(QWizardPage):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.threadpool = QThreadPool()
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()
        self.setTitle(self.tr("Project Fetch"))
        self.setSubTitle(self.tr("Downloading translation files"))

        explainText = QLabel(self)
        explainText.setText(self.tr("We are now attempting to fetch the \
translation files of your project. This step should be pretty fast."))
        explainText.setWordWrap(True)
        layout.addWidget(explainText)

        self.bar = QProgressBar()
        self.bar.setEnabled(True)
        self.bar.setRange(0, 100)
        layout.addWidget(self.bar)

        layout.addSpacing(15)

        pal = QPalette()
        pal.setColor(QPalette.WindowText, Qt.red)

        self.notConfigured = QLabel(self)
        self.notConfigured.setText("The platform is not properly configured, \
please go back.")
        self.notConfigured.setWordWrap(True)
        self.notConfigured.hide()
        self.notConfigured.setPalette(pal)
        layout.addWidget(self.notConfigured)

        self.errorLabel = QLabel(self)
        self.errorLabel.setWordWrap(True)
        self.errorLabel.hide()
        self.errorLabel.setPalette(pal)
        layout.addWidget(self.errorLabel)

        self.finishedLabel = QLabel(self)
        self.finishedLabel.setText(self.tr("Finished!"))
        self.finishedLabel.setWordWrap(True)
        self.finishedLabel.hide()
        layout.addWidget(self.finishedLabel)

        self.setLayout(layout)

        self.check = QCheckBox()
        self.check.setCheckState(Qt.Unchecked)
        self.registerField("downloaded*", self.check)

    def initializePage(self):
        self.check.setCheckState(Qt.Unchecked)
        project = self.field("project")
        lang = self.field("lang")
        email = self.field("email")
        name = self.field("name")
        self.bar.setValue(0)
        self.notConfigured.hide()
        self.errorLabel.hide()
        self.finishedLabel.hide()
        if not self.parent.manager.isConfigured(project['system'], project['info']):
            self.notConfigured.show()
            return

        self.manager = self.parent.manager
        worker = NewRunnable(self, project['name'], lang, project['system'],
                project['info'])
        worker.signals.finished.connect(self.reportFinish)
        worker.signals.progress.connect(self.reportProgress)
        worker.signals.restart_required.connect(self.reportError)
        self.threadpool.start(worker)

    def reportProgress(self, name, progress):
        self.bar.setValue(progress)

    def reportError(self, name, lang, system, info, error):
        self.errorLabel.setText(error)
        self.errorLabel.show()

    def reportFinish(self, name):
        self.bar.setValue(100)
        self.finishedLabel.show()
        self.check.setCheckState(Qt.Checked)

class PageSuccess(QWizardPage):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()
        self.setTitle("Success")
        self.setPixmap(QWizard.WatermarkPixmap, QPixmap(os.path.dirname(__file__) + '/data/illustration.png'))

        explainText = QLabel(self)
        explainText.setText(self.tr("Congratulations! We've just set up your \
first project! Initial setup is now complete, and you can always change the \
settings you've set here from the Offlate welcome screen. After a few \
explanations you will be able to work on your project right away!"))
        explainText.setWordWrap(True)
        layout.addWidget(explainText)
        
        self.setLayout(layout)

    def initializePage(self):
        self.parent.manager.updateSettings({'Generic': {'new': 'False'}})

class PageHints(QWizardPage):
    def __init__(self, parent):
        super().__init__(parent)
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()
        self.setTitle("Offlate Hints")
        self.setPixmap(QWizard.WatermarkPixmap, QPixmap(os.path.dirname(__file__) + '/data/illustration.png'))

        explainManager = QLabel(self)
        explainManager.setText(self.tr("You can always go back to the initial \
screen (called the project manager) from the editor, by using \
<i>file > project manager</i>"))
        explainManager.setWordWrap(True)
        layout.addWidget(explainManager)

        layout.addSpacing(15)

        explainManager = QLabel(self)
        explainManager.setText(self.tr("Quickly switch to the next string with \
<i>Ctrl+Enter</i>. Use <i>Ctrl+Shift+Enter</i> to go back to the previous string \
instead."))
        explainManager.setWordWrap(True)
        layout.addWidget(explainManager)

        layout.addSpacing(15)

        explainManager = QLabel(self)
        explainManager.setText(self.tr("With these hints, you are now ready \
to start working on your project! We will now open the project manager, from \
which you can add more projects, change your settings, etc. Have fun!"))
        explainManager.setWordWrap(True)
        layout.addWidget(explainManager)

        self.setLayout(layout)
