#   Copyright (c) 2019 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class SearchOptions:
    def __init__(self, orig, trans, comm, case, wrap, word):
        self.orig = orig
        self.trans = trans
        self.comm = comm
        self.case = case
        self.wrap = wrap
        self.word = word

class SearchWindow(QDialog):
    def __init__(self, parent = None, replace = False):
        super().__init__(parent)
        self.editor = parent
        self.replace = replace
        self.initUI()

    def showOrHideReplace(self):
        self.replaceField.setVisible(self.replace)
        self.searchRadio.setChecked(not self.replace)
        self.replaceRadio.setChecked(self.replace)
        self.replaceAllButton.setVisible(self.replace)
        self.replaceButton.setVisible(self.replace)
        self.origCheckBox.setChecked(self.origCheckBox.isChecked() and not self.replace)
        self.commCheckBox.setChecked(self.origCheckBox.isChecked() and not self.replace)
        self.origCheckBox.setEnabled(not self.replace)
        self.commCheckBox.setEnabled(not self.replace)

    def setReplace(self, check):
        if not check:
            return
        self.replace = True
        self.showOrHideReplace()

    def setSearch(self, check):
        if not check:
            return
        self.replace = False
        self.showOrHideReplace()

    def _getOptionObject(self):
        return SearchOptions(self.origCheckBox.isChecked(),
                self.transCheckBox.isChecked(), self.commCheckBox.isChecked(),
                self.caseCheckBox.isChecked(), self.wrapCheckBox.isChecked(),
                self.wordCheckBox.isChecked())

    def searchNext(self):
        self.editor.searchNext(self.searchField.text(), self._getOptionObject())

    def searchPrevious(self):
        self.editor.searchPrevious(self.searchField.text(),
                self._getOptionObject())

    def replaceAll(self):
        self.editor.replaceAll(self.searchField.text(), self.replaceField.text(),
                self._getOptionObject())

    def replaceHere(self):
        self.editor.replaceHere(self.searchField.text(), self.replaceField.text(),
                self._getOptionObject())

    def initUI(self):
        vbox = QVBoxLayout()
        self.searchField = QLineEdit()
        self.searchField.setPlaceholderText(self.tr('String to search for'))
        vbox.addWidget(self.searchField)
        self.replaceField = QLineEdit()
        self.replaceField.setPlaceholderText(self.tr('String to replace into'))
        vbox.addWidget(self.replaceField)

        typeBox = QHBoxLayout()
        group = QGroupBox()
        self.searchRadio = QRadioButton(self.tr('Search'))
        self.replaceRadio = QRadioButton(self.tr('Replace'))
        self.searchRadio.clicked.connect(self.setSearch)
        self.replaceRadio.clicked.connect(self.setReplace)
        typeBox.addWidget(self.searchRadio)
        typeBox.addWidget(self.replaceRadio)
        group.setLayout(typeBox)
        vbox.addWidget(group)

        options = QGridLayout()
        self.caseCheckBox = QCheckBox(self.tr('Case sensitive'))
        self.wrapCheckBox = QCheckBox(self.tr('Wrap around'))
        self.wrapCheckBox.setChecked(True)
        self.wordCheckBox = QCheckBox(self.tr('Match whole word only'))
        self.origCheckBox = QCheckBox(self.tr('Search in original text'))
        self.origCheckBox.setChecked(True)
        self.transCheckBox = QCheckBox(self.tr('Search in translated text'))
        self.transCheckBox.setChecked(True)
        self.commCheckBox = QCheckBox(self.tr('Search in comments'))
        options.addWidget(self.caseCheckBox, 0, 0)
        options.addWidget(self.wrapCheckBox, 1, 0)
        options.addWidget(self.wordCheckBox, 2, 0)
        options.addWidget(self.origCheckBox, 0, 1)
        options.addWidget(self.transCheckBox, 1, 1)
        options.addWidget(self.commCheckBox, 2, 1)
        vbox.addLayout(options)

        vbox.addStretch(1)

        buttons = QHBoxLayout()
        closeButton = QPushButton(self.tr('Close'))
        closeButton.clicked.connect(self.close)
        buttons.addWidget(closeButton)
        buttons.addStretch(1)
        self.replaceAllButton = QPushButton(self.tr('Replace all'))
        self.replaceAllButton.clicked.connect(self.replaceAll)
        self.replaceButton = QPushButton(self.tr('Replace one'))
        self.replaceButton.clicked.connect(self.replaceHere)
        previousButton = QPushButton(self.tr('< Previous'))
        previousButton.clicked.connect(self.searchPrevious)
        nextButton = QPushButton(self.tr('Next >'))
        nextButton.setDefault(True)
        nextButton.setAutoDefault(True)
        nextButton.clicked.connect(self.searchNext)
        buttons.addWidget(self.replaceAllButton)
        buttons.addWidget(self.replaceButton)
        buttons.addWidget(previousButton)
        buttons.addWidget(nextButton)
        vbox.addLayout(buttons)

        self.setLayout(vbox)
        self.showOrHideReplace()
