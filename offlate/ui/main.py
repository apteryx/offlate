#   Copyright (c) 2018 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from .manager import ProjectManagerWindow
from .welcome import WelcomeWindow

from ..data.common import REPO, EMAIL

import sys
import os

def main():
    app = QApplication(sys.argv)
    translator = QTranslator()
    if translator.load(QLocale(), "offlate", "_",
            os.path.dirname(os.path.realpath(__file__))+"/../locales"):
        app.installTranslator(translator);

    w = ProjectManagerWindow.getInstance()

    if w.projectManagerWidget.manager.isNew():
        welcome = WelcomeWindow(w.projectManagerWidget.manager)
        welcome.show()
        app.exec_()

    if w.projectManagerWidget.manager.isNew():
        return

    w.projectManagerWidget.manager.setNotNew()
    w.projectManagerWidget.filter()
    try:
        w.show()
        exit = app.exec_()
    except:
        info = sys.exc_info()
        traceback = ''
        trace = info[2]
        while trace is not None:
            traceback = traceback + str(trace.tb_frame) + "\n"
            trace = trace.tb_next
        dialog = QMessageBox()
        dialog.setWindowTitle("Fatal error")
        dialog.setText(
            dialog.tr("The app just crashed, please report the \
following error, with any relevant information (what you did \
when the app crashed, any external factor that might be relevant, \
etc.). You can send your report on {}, or by email to {}.\n\n{}\n\
Traceback:\n{}").format(REPO, EMAIL, info[1], traceback))
        dialog.setStandardButtons(QMessageBox.Close)
        dialog.exec_()
        exit = 1
    sys.exit(exit)
