#   Copyright (c) 2020 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

class ConfigSpec:
    """
    A configuration specification is a list of specifications.  This class
    represents one such specification.
    """
    def __init__(self, key, name, description, optional, link):
        self.key = key
        self.name = name
        self.description = description
        self.optional = optional
        self.link = link

    def isConfigured(self, conf):
        """
        :param dict conf: The configuration
        :returns: Whether this key is present in the configuration and properly
            configured.
        :rtype: bool
        """
        return self.optional or self.key in conf

class StringConfigSpec(ConfigSpec):
    """
    The specification of a string.
    """
    def __init__(self, key, name, description, link=None, placeholder='', optional=False):
        ConfigSpec.__init__(self, key, name, description, optional, link)
        self.placeholder = placeholder

    def isConfigured(self, conf, data):
        return ConfigSpec.isConfigured(self, conf) and \
                (self.optional or (conf[self.key] != None and conf[self.key] != ''))

class ListConfigSpec(ConfigSpec):
    """
    The specification of a list of configurations.
    """
    def __init__(self, key, name, description, specifications, indexKey,
            hasRequiredRow, link=None, optional=False):
        ConfigSpec.__init__(self, key, name, description, optional, link)
        self.specifications = specifications
        self.indexKey = indexKey
        self.hasRequiredRow = hasRequiredRow

    def isConfigured(self, conf, data):
        if self.optional:
            return True

        if not ConfigSpec.isConfigured(self, conf):
            return False

        subconf = conf[self.key]

        if self.hasRequiredRow:
            if not self.hasRequiredRow(data, subconf):
                return False
        
        return True

class RowConfigSpec(ConfigSpec):
    """
    The specification of a row in a list of configurations.
    """
    def __init__(self, key, name, description, specifications, indexKey,
            indexValue, link=None, optional=False):
        ConfigSpec.__init__(self, key, name, description, optional, link)
        self.specifications = specifications
        self.indexKey = indexKey
        self.indexValue = indexValue

    def isConfigured(self, conf, data):
        if self.optional:
            return True

        if not ConfigSpec.isConfigured(self, conf):
            return False

        for conf in self.specifications:
            if not conf.isConfigured(conf, data) and not conf.optional:
                return False

        return True
