<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr" sourcelanguage="">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="../ui/about.py" line="45"/>
        <source>Offlate is a translation interface for offline translation of projects using online platforms. Offlate is free software, you can redistribute it under the GPL v3 license or any later version.</source>
        <translation>Offlate est une interface de traduction hors-ligne de projets qui utilisent des plateformes en ligne. Offlate est un logiciel libre, vous pouvez le redistribuer sous la licence GPL v3 ou toute version ultérieure.</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="53"/>
        <source>Report an issue</source>
        <translation>Rapporter un problème</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="54"/>
        <source>Close this window</source>
        <translation>Fermer cette fenêtre</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="49"/>
        <source>Copyright (C) 2018, 2019 Julien Lepiller</source>
        <translation type="obsolete">Copyright © 2018, 2019 Julien Lepiller
Pour la traduction francophone :
Copyright © 2018, 2019 Julien Lepiller</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="51"/>
        <source>Copyright (C) 2018-2021 Julien Lepiller</source>
        <translation>Copyright (C) 2018-2021 Julien Lepiller</translation>
    </message>
</context>
<context>
    <name>AdvancedProjectWidget</name>
    <message>
        <location filename="../ui/new.py" line="95"/>
        <source>Project information</source>
        <translation type="unfinished">Informations sur le projet</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="103"/>
        <source>Name:</source>
        <translation type="unfinished">Nom :</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="104"/>
        <source>Target Language:</source>
        <translation type="unfinished">Langue cible :</translation>
    </message>
</context>
<context>
    <name>CopyrightSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="139"/>
        <source>In some cases, we need to add a copyright line for you in the translation files we send upstream. Here, you can configure how you want the copyright line to look like. This information will most likely become public after your first contribution.</source>
        <translation>Dans certains cas, nous avons besoin d&apos;ajouter une ligne concernant vos droits d&apos;auteur dans les fichiers de traduction que nous envoyons en amont. Ici, vous pouvez configurer la manière dont vous voulez que cette ligne d&apos;information apparaisse. Ces informations seront probablement publiques après votre première contribution.</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="186"/>
        <source>John Doe</source>
        <translation>Jean Dupont</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="188"/>
        <source>john@doe.me</source>
        <translation>jean@dupont.me</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="160"/>
        <source>Here is how your copyright line will look like:</source>
        <translation>Voici ce à quoi votre ligne d&apos;information ressemblera :</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="189"/>
        <source>Copyright &amp;copy; {} {} &amp;lt;{}&amp;gt;</source>
        <translation>Copyright &amp;copy; {} {} &amp;lt;{}&amp;gt;</translation>
    </message>
</context>
<context>
    <name>EditorWindow</name>
    <message>
        <location filename="../ui/editor.py" line="529"/>
        <source>Unsupported / Unknown project</source>
        <translation>Projet inconnu ou non supporté</translation>
    </message>
    <message numerus="yes">
        <location filename="../ui/editor.py" line="551"/>
        <source>{} translated on {} total ({}%).</source>
        <translation>
            <numerusform>{} traduit sur {} au total ({} %).</numerusform>
            <numerusform>{} traduits sur {} au total ({} %).</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="650"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="652"/>
        <source>Exit application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="655"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="657"/>
        <source>Save current project</source>
        <translation>Sauvegarder le projet actuel</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="660"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="662"/>
        <source>New project</source>
        <translation>Nouveau projet</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="665"/>
        <source>Manage Projects</source>
        <translation>Gérer les projets</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="668"/>
        <source>Open project manager</source>
        <translation>Ouvrir le gestionnaire de projets</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="671"/>
        <source>Update</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="673"/>
        <source>Get modifications from upstream</source>
        <translation>Récupérer les modifications en amont</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="676"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="677"/>
        <source>Close current project</source>
        <translation>Fermer le projet actuel</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="680"/>
        <source>Send</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="682"/>
        <source>Send modifications upstream</source>
        <translation>Envoyer les modifications en amont</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="685"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="687"/>
        <source>Set parameters</source>
        <translation>Configurer les paramètres</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="700"/>
        <source>Show Translated</source>
        <translation>Montrer les chaînes traduites</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="703"/>
        <source>Show Fuzzy</source>
        <translation>Montrer les traductions floues</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="706"/>
        <source>Show Empty Translation</source>
        <translation>Montrer les traductions vides</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="709"/>
        <source>Use a monospace font</source>
        <translation>Utiliser une police à chasse fixe</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="734"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="738"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="746"/>
        <source>&amp;Project</source>
        <translation>&amp;Projet</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="753"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="758"/>
        <source>&amp;View</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="563"/>
        <source>Uploading {}...</source>
        <translation>Envoi de {}…</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="570"/>
        <source>Finished uploading {}!</source>
        <translation>Fin de l&apos;envoi de {} !</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="573"/>
        <source>Updating {}...</source>
        <translation>Mise à jour de {}…</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="585"/>
        <source>Finished updating {}!</source>
        <translation>Fin de la mise à jour de {} !</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="690"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="692"/>
        <source>Search in the document</source>
        <translation>Rechercher dans le document</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="695"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="697"/>
        <source>Replace content in the document</source>
        <translation>Remplacer le contenu dans le document</translation>
    </message>
</context>
<context>
    <name>GitlabEdit</name>
    <message>
        <location filename="../ui/gitlabedit.py" line="59"/>
        <source>server</source>
        <translation>serveur</translation>
    </message>
    <message>
        <location filename="../ui/gitlabedit.py" line="61"/>
        <source>token</source>
        <translation>jeton</translation>
    </message>
    <message>
        <location filename="../ui/gitlabedit.py" line="62"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../ui/gitlabedit.py" line="64"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>LangSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="204"/>
        <source>Which language will you most frequently translate projects into? This setting can be overriden in each individual projects.</source>
        <translation>Vers quelle langue traduirez-vous vos projets le plus fréquemment? Ce paramètre peut être changé individuellement dans chaque projet.</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="211"/>
        <source>None</source>
        <translation>Aucune</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="229"/>
        <source>Note that you should only translate into native language or to a language you are extremely familiar with, to avoid weird or nonsensical translations.</source>
        <translation>Remarquez que vous ne devriez traduire que vers votre langue natale ou vers une langue que vous connaissez très bien, pour éviter les traduction maladroites ou les contre-sens.</translation>
    </message>
</context>
<context>
    <name>ListSettingsEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="142"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../ui/settingsedit.py" line="144"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>ListSettingsRowEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="53"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MultipleLineEdit</name>
    <message>
        <location filename="../ui/multiplelineedit.py" line="59"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../ui/multiplelineedit.py" line="61"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>NewWindow</name>
    <message>
        <location filename="../ui/new.py" line="68"/>
        <source>The Translation Project</source>
        <translation type="obsolete">Le Projet de Traduction</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="228"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="229"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="57"/>
        <source>Project information</source>
        <translation type="obsolete">Informations sur le projet</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="65"/>
        <source>Name:</source>
        <translation type="obsolete">Nom :</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="66"/>
        <source>Target Language:</source>
        <translation type="obsolete">Langue cible :</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="69"/>
        <source>Transifex</source>
        <translation type="obsolete">Transifex</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="100"/>
        <source>Organization</source>
        <translation type="obsolete">Organisation</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="70"/>
        <source>Gitlab</source>
        <translation type="obsolete">Gitlab</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="122"/>
        <source>repository</source>
        <translation type="obsolete">dépôt</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="127"/>
        <source>branch</source>
        <translation type="obsolete">branche</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="71"/>
        <source>Github</source>
        <translation type="obsolete">Github</translation>
    </message>
</context>
<context>
    <name>PageCopyright</name>
    <message>
        <location filename="../ui/welcome.py" line="98"/>
        <source>Copyright Settings</source>
        <translation>Paramètres des droits d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="99"/>
        <source>Configuring how your copyright is added to files</source>
        <translation>Configuration de la manière d&apos;ajouter vos droits d&apos;auteur aux fichiers</translation>
    </message>
</context>
<context>
    <name>PageDownload</name>
    <message>
        <location filename="../ui/welcome.py" line="188"/>
        <source>Project Fetch</source>
        <translation>Récupération du projet</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="189"/>
        <source>Downloading translation files</source>
        <translation>Téléchargement des fichiers de configuration</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="192"/>
        <source>We are now attempting to fetch the translation files of your project. This step should be pretty fast.</source>
        <translation>Nous essayons maintenant de récupérer les fichiers de traduction de votre projet. Cette étape devrait être assez rapide.</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="222"/>
        <source>Finished!</source>
        <translation>Terminé !</translation>
    </message>
</context>
<context>
    <name>PageHints</name>
    <message>
        <location filename="../ui/welcome.py" line="302"/>
        <source>You can always go back to the initial screen (called the project manager) from the editor, by using &lt;i&gt;file &gt; project manager&lt;/i&gt;</source>
        <translation>Vous pouvez toujours revenir à l&apos;écran d&apos;accueil (appelé le gestionnaire de projets) depuis l&apos;éditeur, en utilisant &lt;i&gt; fichier &gt; gestionnaire de projets&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="311"/>
        <source>Quickly switch to the next string with &lt;i&gt;Ctrl+Enter&lt;/i&gt;. Use &lt;i&gt;Ctrl+Shift+Enter&lt;/i&gt; to go back to the previous string instead.</source>
        <translation>Passez rapidement à la chaine suivante avec &lt;i&gt;Ctrl+Entrée&lt;/i&gt;. Utilisez &lt;i&gt;Ctrl+Shift+Entrée&lt;/i&gt; pour revenir à la chaine précédente.</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="320"/>
        <source>With these hints, you are now ready to start working on your project! We will now open the project manager, from which you can add more projects, change your settings, etc. Have fun!</source>
        <translation>Avec ces astuces, vous voilà maintenant prêt·e à travailler sur votre projet ! Nous allons maintenant ouvrir le gestionnaire de projets, à partir duquel vous pouvez ajouter plus de projets, changer vos paramètres, etc. Amusez-vous bien !</translation>
    </message>
</context>
<context>
    <name>PageLang</name>
    <message>
        <location filename="../ui/welcome.py" line="117"/>
        <source>Language Settings</source>
        <translation>Paramètre linguistiques</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="118"/>
        <source>Configuring the default language for new projects</source>
        <translation>Configuration de la langue par défaut des nouveaux projets</translation>
    </message>
</context>
<context>
    <name>PageProjectSelection</name>
    <message>
        <location filename="../ui/welcome.py" line="140"/>
        <source>First Project</source>
        <translation>Premier projet</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="141"/>
        <source>Choosing a first project</source>
        <translation>Choix d&apos;un premier projet</translation>
    </message>
</context>
<context>
    <name>PageSuccess</name>
    <message>
        <location filename="../ui/welcome.py" line="279"/>
        <source>Congratulations! We&apos;ve just set up your first project! Initial setup is now complete, and you can always change the settings you&apos;ve set here from the Offlate welcome screen. After a few explanations you will be able to work on your project right away!</source>
        <translation>Félicitations ! Nous venons tout juste de configurer votre premier projet ! La configuration initiale est maintenant terminée et vous pouvez toujours changer les paramètres que vous venez d&apos;initialiser ici à partir de l&apos;écran d&apos;accueil d&apos;Offlate. Après quelques explications vous pourrez travailler sur votre projet immédiatement !</translation>
    </message>
</context>
<context>
    <name>PageSystemSettings</name>
    <message>
        <location filename="../ui/welcome.py" line="163"/>
        <source>System Configuration</source>
        <translation>Configuration du système</translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="164"/>
        <source>Getting an account on the project&apos;s platform</source>
        <translation>Création d&apos;un compte sur la plateforme du projet</translation>
    </message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <location filename="../ui/welcome.py" line="81"/>
        <source>Offlate is a tool to help you localise free and open source software. Before you start contributing translations to a project though, there are a few things we need to set up and talk about. Let&apos;s get started!</source>
        <translation>Offlate est un outil qui vous aide à traduire les logiciels libres et ouverts. Avant de commencer à contribuer des traductions pour un projet, il y a cependant quelques petites choses que nous devons paramétrer et dont nous devons parler. C&apos;est parti !</translation>
    </message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../systems/gitlab.py" line="92"/>
        <source>Repository</source>
        <translation>Dépôt</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="92"/>
        <source>Full clone URL for the repository</source>
        <translation>URL complète pour le clonage du dépôt</translation>
    </message>
    <message>
        <location filename="../systems/github.py" line="89"/>
        <source>https://...</source>
        <translation>https://…</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="95"/>
        <source>Branch</source>
        <translation>Branche</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="95"/>
        <source>Name of the branch to translate</source>
        <translation>Nom de la branche à traduire</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="95"/>
        <source>master</source>
        <translation>master</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>Token</source>
        <translation>Jeton</translation>
    </message>
    <message>
        <location filename="../systems/github.py" line="98"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://github.com/settings/tokens/new&lt;/a&gt;. You will need at least to grant the public_repo permission.</source>
        <translation>Vous pouvez récupérer un jeton sur &lt;a href=&quot;#&quot;&gt;https://github.com/settings/tokens/new&lt;/a&gt;. Vous devrez au moins donner la permission public_repo.</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="105"/>
        <source>Gitlab instance configuration</source>
        <translation>Configuration de l&apos;instance Gitlab</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="105"/>
        <source>You need to configure each Gitlab instance separately, and you haven&apos;t configured the instance at {} yet.</source>
        <translation>Vous devez configurer chaque instance Gitlab séparément, et vous n&apos;avez pas encore configuré l&apos;instance {}.</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="105"/>
        <source>The token you created from your account. You can create it from &lt;a href=&quot;#&quot;&gt;the Access Tokens tab&lt;/a&gt; in your account settings.</source>
        <translation>Le jeton que vous avez créé depuis votre compte. Vous pouvez le créer à partir de &lt;a href=&quot;#&quot;&gt;l&apos;onglet Access Tokens&lt;/a&gt; de vos paramètres de compte.</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="119"/>
        <source>Configured Gitlab instances</source>
        <translation>Instances Gitlab configurées</translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="119"/>
        <source>You need to create a token for each Gitlab instance you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation>Vous devez créer un jeton pour chaque instance Gitlab sur lesquelles vous possédez un compte. Vous pouvez créer un jeton en vous connectant à votre compte, en allant dans vos paramètres et sur la page Access Tokens.</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>Server</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>Server name</source>
        <translation>Nom du serveur</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>The token you created from your account</source>
        <translation>Le jeton que vous avez créé pour votre compte</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="153"/>
        <source>version</source>
        <translation>version</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="153"/>
        <source>version of the project (keep empty for latest)</source>
        <translation>version du projet (laissez vide pour la dernière version)</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="159"/>
        <source>Mail server</source>
        <translation>Serveur de courriel</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="159"/>
        <source>To send your work to the translation project on your behalf, we need to know the email server you are going to use (usually the part on the right of the `@` in your email address).</source>
        <translation>Pour envoyer votre travail au projet de traduction à votre place, nous devons connaitre le serveur de courriel que vous utiliser (habituellement la partie à droite du `@&apos; dans votre adresse de courriel).</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="159"/>
        <source>example.com</source>
        <translation>example.com</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="164"/>
        <source>Mail user</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="164"/>
        <source>Username used to connect to your mail server, usually the email address itself, or the part of the left of the `@`.</source>
        <translation>Le nom d&apos;utilisateur utilisé pour vous connecter à votre serveur de courriel, habituellement l&apos;adresse de courriel entière ou la partie à gauche du `@&apos;.</translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="164"/>
        <source>john</source>
        <translation>jean</translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="147"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="unfinished">Vous pouvez récupérer un jeton sur &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="153"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="153"/>
        <source>The organization this project belongs in, on transifex.</source>
        <translation>L&apos;organisation à laquelle ce projet appartient, sur transifex.</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="218"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="155"/>
        <source>The name of the project on transifex</source>
        <translation>Le nom du projet sur transifex</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="215"/>
        <source>Instance</source>
        <translation>Instance</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="215"/>
        <source>Full clone URL of the Weblate instance</source>
        <translation type="obsolete">URL de l&apos;instance Weblate</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>https://weblate.org</source>
        <translation>https://hosted.weblate.org</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="218"/>
        <source>Name of the project on the instance</source>
        <translation>Nom du projet sur l&apos;instance</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="218"/>
        <source>foo</source>
        <translation>toto</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="225"/>
        <source>Weblate instance configuration</source>
        <translation>Configuration de l&apos;instance Weblate</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="225"/>
        <source>You need to configure each Weblate instance separately, and you haven&apos;t configured the instance at {} yet.</source>
        <translation>Vous devez configurer chaque instance Weblate séparément, et vous n&apos;avez par encore configuré l&apos;instance {}.</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="225"/>
        <source>The token you created from your account. You can create it from &lt;a href=&quot;#&quot;&gt;the API access tab&lt;/a&gt; in your account settings.</source>
        <translation>Le jeton que vous avez créé depuis votre compte. Vous pouvez le créer à partir de &lt;a href=&quot;#&quot;&gt;l&apos;onglet API access&lt;/a&gt; dans vos paramètres de compte.</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM</source>
        <translation>fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>Configured Weblate instances</source>
        <translation>Instances Weblate configurées</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="236"/>
        <source>You need to find a token for each Weblate instance you have an account on. You can create a token by logging into your account, going to your settings and in the API Access page.</source>
        <translation>Vous devez trouver un jeton pour chaque instance Weblate sur lesquelles vous avez un compte. Vous pouvez créer un jeton en vous connectant à votre compte, en allant dans vos paramètres et sur la page API Access.</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="215"/>
        <source>URL of the Weblate instance</source>
        <translation>URL de l&apos;instance Weblate</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="215"/>
        <source>https://hosted.weblate.org</source>
        <translation>https://hosted.weblate.org</translation>
    </message>
</context>
<context>
    <name>ProjectManagerWidget</name>
    <message>
        <location filename="../ui/manager.py" line="89"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="90"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="91"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="99"/>
        <source>New Project</source>
        <translation>Nouveau projet</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="100"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="101"/>
        <source>About Offlate</source>
        <translation>À propos d&apos;Offlate</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="102"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="219"/>
        <source>Fetching project {}...</source>
        <translation>Récupération du projet {}…</translation>
    </message>
</context>
<context>
    <name>ProjectManagerWindow</name>
    <message>
        <location filename="../ui/manager.py" line="53"/>
        <source>Offlate Project Manager</source>
        <translation>Gestionnaire de projets d&apos;Offlate</translation>
    </message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <location filename="../ui/editor.py" line="296"/>
        <source>Singular</source>
        <translation>Singulier</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="297"/>
        <source>Plural</source>
        <translation>Pluriel</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="157"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="153"/>
        <source>Open in external editor</source>
        <translation>Ouvrir dans un éditeur externe</translation>
    </message>
</context>
<context>
    <name>SearchWindow</name>
    <message>
        <location filename="../ui/search.py" line="84"/>
        <source>String to search for</source>
        <translation>Chaine à rechercher</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="87"/>
        <source>String to replace into</source>
        <translation>Chaine à utiliser à la place</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="92"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="93"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="102"/>
        <source>Case sensitive</source>
        <translation>Sensible à la casse</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="103"/>
        <source>Wrap around</source>
        <translation>Faire le tour</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="105"/>
        <source>Match whole word only</source>
        <translation>Trouver seulement des mots complets</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="106"/>
        <source>Search in original text</source>
        <translation>Rechercher dans le texte d&apos;origine</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="108"/>
        <source>Search in translated text</source>
        <translation>Rechercher dans la traduction</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="110"/>
        <source>Search in comments</source>
        <translation>Rechercher dans les commentaires</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="122"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="126"/>
        <source>Replace all</source>
        <translation>Tout remplacer</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="128"/>
        <source>Replace one</source>
        <translation>Remplacer une fois</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="130"/>
        <source>&lt; Previous</source>
        <translation>&lt; Précédent</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="132"/>
        <source>Next &gt;</source>
        <translation>Suivant &gt;</translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="287"/>
        <source>Configure me</source>
        <translation>Configurez-moi</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="308"/>
        <source>Done!</source>
        <translation>Terminé !</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../ui/settings.py" line="42"/>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="43"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="129"/>
        <source>Email:</source>
        <translation type="obsolete">Adresse de courriel :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="130"/>
        <source>Server:</source>
        <translation type="obsolete">Serveur :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="131"/>
        <source>User Name:</source>
        <translation type="obsolete">Nom d&apos;utilisateur :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="56"/>
        <source>Transifex</source>
        <translation type="obsolete">Transifex</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="68"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="obsolete">Vous pouvez récupérer un jeton sur &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="164"/>
        <source>Token:</source>
        <translation type="obsolete">Jeton :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="108"/>
        <source>Translation Project</source>
        <translation type="obsolete">Projet de traduction</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="178"/>
        <source>Gitlab</source>
        <translation type="obsolete">Gitlab</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="180"/>
        <source>Add your gitlab account tokens below. You need to create a token for every gitlab server you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation type="obsolete">Ajoutez les jetons de vos comptes gitlab ci-dessous. Vous devrez créer un jeton par serveur gitlab sur lequel vous avez un compte. Vous pouvez créer un jeton en vous connectant à votre compte, en allant dans les paramètres et sur la page jetons d&apos;accès.</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="86"/>
        <source>Generic Settings</source>
        <translation type="obsolete">Paramètres généraux</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="89"/>
        <source>John Doe &lt;john@doe.me&gt;</source>
        <translation type="obsolete">Jean Dupont &lt;jean@dupont.fr&gt;</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="96"/>
        <source>Full Name:</source>
        <translation type="obsolete">Nom complet :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="101"/>
        <source>Generic</source>
        <translation type="obsolete">Général</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="147"/>
        <source>Github</source>
        <translation type="obsolete">Github</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="159"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://github.com/settings/tokens/new&lt;/a&gt;.             You will need at least to grant the public_repo permission.</source>
        <translation type="obsolete">Vous pouvez récupérer un jeton sur &lt;a href=&quot;#&quot;&gt;https://github.com/settings/tokens/new&lt;/a&gt;.
Vous aurez besoin d&apos;au moins la permission public_repo.</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="42"/>
        <source>Offlate Settings</source>
        <translation>Paramètres d&apos;Offlate</translation>
    </message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="../ui/spellcheckedit.py" line="47"/>
        <source>Spelling Suggestions</source>
        <translation>Suggestions</translation>
    </message>
    <message>
        <location filename="../ui/spellcheckedit.py" line="48"/>
        <source>No Suggestions</source>
        <translation>Pas de suggestion</translation>
    </message>
</context>
<context>
    <name>SystemSettingsWindow</name>
    <message>
        <location filename="../ui/config/settings.py" line="116"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>WelcomeWindow</name>
    <message>
        <location filename="../ui/config/welcome.py" line="38"/>
        <source>Welcome to Offlate</source>
        <translation>Bienvenue dans Offlate</translation>
    </message>
</context>
<context>
    <name>dialog</name>
    <message>
        <location filename="../ui/main.py" line="60"/>
        <source>The app just crashed, please report the following error, with any relevant information (what you did when the app crashed, any external factor that might be relevant, etc.). You can send your report on {}, or by email to {}.

{}
Traceback:
{}</source>
        <translation>L&apos;appli vient de crasher, veuillez rapporter l&apos;erreur suivante avec toutes les informations utiles (ce que vous faisiez au moment du plantage, les facteurs externes qui pourraient avoir un rapport, etc). Vous pouvez écrire votre rapport sur {} ou l&apos;envoyer par courriel à {}.  {} Traceback: {}</translation>
    </message>
</context>
<context>
    <name>self.parent</name>
    <message>
        <location filename="../ui/parallel.py" line="57"/>
        <source>A project with the same name already exists. The new project was not created. You should first remove the same-named project.</source>
        <translation>Il existe déjà un projet avec le même nom. Le nouveau projet n&apos;a pas été créé. Vous devriez supprimer le projet qui a le même nom.</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="61"/>
        <source>Your filesystem contains a same-named directory for your new project. The new project was not created. You should first remove the same-named directory: &quot;{}&quot;.</source>
        <translation>Votre système de fichiers contient déjà un répertoire avec le même nom que votre nouveau projet. Le nouveau projet n&apos;a pas été créé. Vous devriez d&apos;abord supprimer le répertoire avec le même nom : « {} ».</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="67"/>
        <source>The project you added uses the {} format, but it is not supported yet by Offlate. You can try to update the application, or if you are on the latest version already, report it as a bug.</source>
        <translation>Le projet que vous avez ajouté utilise le format {}, mais il n&apos;est pas encore pris en charge par Offlate. Vous pouvez essayer de mettre à jour l&apos;application, ou si vous êtes déjà sur la dernière version, rapportez un bogue.</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="75"/>
        <source>An unexpected error occured while fetching the project: {}. You should report this as a bug.</source>
        <translation>Une erreur inattendue a eu lieue lors de la récupération du projet : {}. Vous devriez rapporter un bogue.</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="71"/>
        <source>The project {} you added could not be found in the translation platform you selected. Did you make a typo while entering the name or other parameters?</source>
        <translation>Le projet {} que vous avez ajouté n&apos;a pas été trouvé sur la plateforme de traduction de vous avez choisie. Avez-vous fait une coquille en tapant son nom ou d&apos;autres paramètres ?</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="48"/>
        <source>This action did not complete correctly. Try again, and if the issue persists, consider sending a bug report at {}, or by email to {} with the following information, and any relevant information (what you were trying to do, other external factors, etc.) We received the following error message: {}.

Traceback:

{}
Try again?</source>
        <translation>Cette action ne s&apos;est pas terminée correctement. Essayez de nouveau et si l&apos;erreur persiste, envoyez un rapport de bug sur {} ou par courriel à {} avec les informations suivante et toute information pertinent (ce que vous essayiez de faire, d&apos;autres facteurs externes, etc). Nous avons reçu le message d&apos;erreur suivant : {}.  Traceback:  {} Essayez de nouveau ?</translation>
    </message>
</context>
<context>
    <name>self.qd</name>
    <message>
        <location filename="../ui/editor.py" line="55"/>
        <source>Please enter your password:</source>
        <translation>Saisissez votre mot de passe :</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="63"/>
        <source>Token for {} not found. Have you added this server to your settings?.</source>
        <translation>Jeton pour {} introuvable. Avez-vous ajouté ce serveur à vos paramètres ?</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="73"/>
        <source>Error while creating branch {}.</source>
        <translation>Erreur lors de la création de la branche {}.</translation>
    </message>
</context>
</TS>
