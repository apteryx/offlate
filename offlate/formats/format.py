#   Copyright (c) 2020 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

class Format:
    """
    The base class for translation formats.

    A format is a class that is linked to a project. Each instance of this
    class represents the files that contain the source and target strings
    in a specific format (gettext, yaml, json, ...).
    """

    def __init__(self, conf):
        self.conf = conf

    def content(self):
        """
        Return the content of this set of source and target strings.

        You may use these as read-write object.  Any change to the content
        can later be saved using the save method.

        :returns: List of entries
        :rtype: Entry list
        """
        raise Exception("Unimplemented method in concrete class: content")

    def save(self):
        """
        Saves the content to files.

        This method assumes that the content (Entry objects) is modified
        in-place, and will save their content.

        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: save")

    def merge(self, older, callback):
        """
        Merge two versions of the same translation into this translation set
        and save the result.

        :param Format older: The previous version of the translation
        :param FormatCallback callback: A format callback to help deal with
            merge conflicts.
        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: merge")

    def reload(self):
        """
        Reloads the content from the files without saving existing changes.

        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: reload")

    def getExternalFiles(self):
        """
        :returns: The list of files this instance may read or write
        :rtype: String list
        """
        raise Exception("Unimplemented method in concrete class: getExternalFiles")

    def getTranslationFiles(self):
        """
        A format can be composed of different files.  For instance, a file
        that contains the English (or base) language version of the strings,
        and a file that contains the translated strings.

        :returns: The list of files that contain the actual translation
        :rtype: String list
        """
        raise Exception("Unimplemented method in concrete class: getTranslationFiles")
