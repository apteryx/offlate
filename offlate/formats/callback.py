#   Copyright (c) 2020 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

class FormatCallback:
    """
    The base class for a proper FormatCallback class.
    """
    def mergeConflict(self, base, oldTranslation, newTranslation):
        """
        Resolve a merge conflict.  This method is called when merging two
        Format instances.  When a base string is translated in two different
        (non-empty) ways, a conflict arrises.

        :returns: The translation that will be used for this entry.
        :rtype: str
        """
        raise Exception("Unimplemented method in concrete class: mergeConflict")
