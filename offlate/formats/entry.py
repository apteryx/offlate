#   Copyright (c) 2018, 2019, 2020 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

class Entry:
    """
    Set of very related pairs of base language strings and their translations.
    Most of the time, and entry contains only one base language string and one
    translation.  Some formats handle plurals, and each variant is saved as an
    additional string in the same entry.
    """
    def __init__(self, msgids, msgstrs, fuzzy, obsolete, info = []):
        """
        Create an entry.

        :param strs msgids: the set of base language strings
        :param strs msgstrs: the set of target language strings (translated)
        :param bool fuzzy: whether the entry is fuzzy
        :param bool obsolete: whether the entry is obsolete
        :param dicts info: structured information about this entry
        """
        self.msgids = msgids
        self.msgstrs = msgstrs
        self.fuzzy = fuzzy
        self.obsolete = obsolete
        self.info = info

    def isTranslated(self):
        """
        :returns: Whether the entry is fully translated
        :rtype: bool
        """
        for msgstr in self.msgstrs:
            if msgstr == '':
                return False
        return True

    def isFuzzy(self):
        """
        :returns: Whether the entry is fuzzy
        :rtype: bool
        """
        return self.fuzzy

    def isObsolete(self):
        """
        :returns: Whether the entry is obsolete
        :rtype: bool
        """
        return self.obsolete

    def getInfo(self):
        """
        :returns: Structured information about this entry (location, comments, ...)
        :rtype: dicts
        """
        return self.info

    def update(self, index, content):
        """
        Update the entry by replacing the translation number index with content.

        :rtype: None
        """
        self.msgstrs[index] = content

    def get(self, index):
        """
        :returns: The indexth translation in this entry.
        :rtype: str
        """
        if isinstance(self.msgstrs, list):
            return self.msgstrs[index]
        else:
            return list(self.msgstrs.items())[index][1]

    def isPlural(self):
        """
        :retuns: Whether the entry is an entry for a plural form.
        :rtype: bool
        """
        return len(self.msgstrs) > 1

class POEntry(Entry):
    """
    An entry for the gettext format.
    """
    def __init__(self, entry):
        msgids = [entry.msgid]
        msgstrs = [entry.msgstr]
        if 0 in entry.msgstr_plural:
            msgstrs = []
            for msgstr in entry.msgstr_plural:
                msgstrs.append(entry.msgstr_plural[msgstr])
            msgids = [entry.msgid, entry.msgid_plural]
        info = []
        for o in entry.occurrences:
            info.append({'type': 'location', 'file': o[0], 'line': o[1]})
        if entry.comment is not None and entry.comment != '':
            info.append({'type': 'comment', 'comment': entry.comment})
        if entry.tcomment is not None and entry.tcomment != '':
            info.append({'type': 'comment', 'comment': entry.tcomment})
        Entry.__init__(self, msgids, msgstrs, "fuzzy" in entry.flags, entry.obsolete, info)
        self.entry = entry

    def update(self, index, content):
        Entry.update(self, index, content)
        self.fuzzy = False
        self.entry.flags = [x for x in self.entry.flags if x != 'fuzzy']
        if 0 in self.entry.msgstr_plural:
            self.entry.msgstr_plural[index] = content
        else:
            self.entry.msgstr = content

class AndroidStringsEntry(Entry):
    """
    An entry for the android translation format.
    """
    def __init__(self, entry, parent=None, index=None):
        if entry.type == 'string':
            msgids = [entry.orig]
            msgstrs = [entry.dst]
        else:
            msgids = entry.orig
            msgstrs = entry.dst
        Entry.__init__(self, msgids, msgstrs, False, False)
        self.entry = entry
        self.parent = parent
        self.index = index

    def update(self, index, content):
        Entry.update(self, index, content)
        self.fuzzy = False
        if self.entry.type == 'plurals':
            self.entry.dst[index] = content
        else:
            self.entry.dst = content
            if self.parent is not None:
                self.parent.dst[self.index] = content

    def isPlural(self):
        return isinstance(self.msgstrs, dict)

class AppstoreEntry(Entry):
    """
    An entry for the android app description format.
    """
    def __init__(self, filename, en, tr):
        Entry.__init__(self, [en], [tr], False, False)
        self.en = en
        self.tr = tr
        self.filename = filename

    def update(self, index, content):
        Entry.update(self, index, content)
        self.tr = content


class JSONEntry(Entry):
    """
    An entry for the json format.
    """
    def __init__(self, entry):
        Entry.__init__(self, [entry['source_string']], [entry['translation']], False, False)
        self.entry = entry

    def update(self, index, content):
        Entry.update(self, index, content)
        self.entry['translation'] = content

class YAMLEntry(Entry):
    """
    An entry for the yaml format.
    """
    def __init__(self, entry):
        self.entry = entry
        Entry.__init__(self, [entry['source_string']],
                [entry['translation']], False, False)

    def update(self, index, content):
        Entry.update(self, index, content)
        self.entry['translation'] = content

class TSEntry(Entry):
    def __init__(self, entry):
        self.entry = entry
        numerus = entry.get('numerus') == 'yes'
        sourcestring = ""
        translation = [] if numerus else ""
        translationtype = None
        for child in entry:
            if child.tag == "source":
                sourcestring = child.text
            elif child.tag == "translation":
                translationtype = child.get('type')
                if numerus:
                    for form in child:
                        txt = form.text
                        translation.append("" if txt is None else txt)
                else:
                    txt = child.text
                    translation = "" if txt is None else txt
        if numerus:
            sourcestring = [sourcestring, sourcestring]
        else:
            translation = [translation]
            sourcestring = [sourcestring]
        fuzzy = False
        obsolete = False
        if translationtype == 'obsolete':
            obsolete = True
        if translationtype == 'unfinished':
            fuzzy = True
        Entry.__init__(self, sourcestring, translation, fuzzy, obsolete)

    def update(self, index, content):
        Entry.update(self, index, content)
        numerus = self.entry.get('numerus') == 'yes'
        for child in self.entry:
            if child.tag == "translation":
                if numerus:
                    i=0
                    self.fuzzy = False
                    for form in child:
                        if i == index:
                            form.text = content
                        if form.text == '':
                            self.fuzzy = True
                        i = i + 1
                else:
                    child.text = content
                    self.fuzzy = content == ""
