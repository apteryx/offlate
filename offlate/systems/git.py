#   Copyright (c) 2018, 2019 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
""" The git abstract system connector. """

import pygit2
from os import rename
from pathlib import Path

from translation_finder import discover
from ..core.config import StringConfigSpec
from ..formats.gettext import GettextFormat
from ..formats.ts import TSFormat
from ..formats.yaml import YamlFormat
from ..formats.androidstrings import AndroidStringsFormat
from ..formats.appstore import AppstoreFormat
from ..formats.exception import UnsupportedFormatException
from .exception import ProjectNotFoundSystemException
from .project import Project

def rmdir(dir):
    dir = Path(dir)
    for item in dir.iterdir():
        if item.is_dir():
            rmdir(item)
        else:
            item.unlink()
    dir.rmdir()

class Progress(pygit2.RemoteCallbacks):
    def __init__(self, callback):
        super().__init__()
        self.callback = callback

    def transfer_progress(self, stats):
        if self.callback is not None:
            self.callback.reportProgress((100.0 * stats.received_objects) / \
                    stats.total_objects)

class GitProject(Project):
    def __init__(self, name, lang, conf, data = {}):
        Project.__init__(self, name, lang, conf, data)

    def open(self, basedir):
        self.basedir = basedir
        self._updateURI()
        self._updateFiles()

    def initialize(self, basedir, callback=None):
        self.basedir = basedir
        self._updateURI()
        self._clone(basedir + "/current", callback)
        self._updateFiles()
    
    def _updateURI(self):
        raise Exception("Unimplemented method in concrete class: _updateURI")

    def _updateFiles(self):
        Project.translationfiles = self._updateFilesFromDirectory(
                self.basedir + '/current')

    def _updateFilesFromDirectory(self, directory):
        translations = discover(directory)
        translationfiles = []
        path = directory + '/'
        for resource in translations:
            if resource['file_format'] == 'po':
                popath = resource['filemask'].replace('*', self.lang)
                if 'new_base' in resource:
                    potpath = resource['new_base']
                else:
                    # If there is no POT, then we can't really do anything...
                    continue
                translationfiles.append({'filename': popath,
                    'format': GettextFormat({'file': path + popath,
                        'pot': path + potpath,
                        'version': self.conf['offlate_version'],
                        'fullname': self.conf['name'] + ' <' + self.conf['email'] + '>',
                        'lang': self.lang})})
            elif resource['file_format'] == 'yaml':
                yamlpath = resource['filemask'].replace('*', self.lang)
                translationfiles.append({'filename': yamlpath,
                    'format': YamlFormat({'dest': path + yamlpath,
                        'source': path + resource['template']})})
            elif resource['file_format'] == 'ts':
                yamlpath = resource['filemask'].replace('*', self.lang)
                enpath = resource['filemask'].replace('*', 'en')
                template = None
                if Path(path + enpath).exists():
                    template = enpath
                else:
                    template = Path(path).glob(resource['filemask'])[0]
                translationfiles.append({'filename': yamlpath,
                    'format': TSFormat({'file': path + yamlpath, 'lang': self.lang, 'template': template})})
            elif resource['file_format'] == 'aresource':
                translationpath = resource['filemask'].replace('*', self.lang)
                enpath = resource['template']
                translationfiles.append({'filename': translationpath,
                    'format': AndroidStringsFormat({'file': path + translationpath, 'lang': self.lang,
                                                    'template': path + enpath})})
            elif resource['file_format'] == 'appstore':
                translationpath = resource['filemask'].replace('*', self.lang)
                enpath = resource['template']
                translationfiles.append({'filename': translationpath,
                    'format': AppstoreFormat({'file': path + translationpath, 'lang': self.lang,
                                              'template': path + enpath})})
            else:
                raise UnsupportedFormatException(resource['file_format'])
        return translationfiles

    def _clone(self, directory, callback=None):
        try:
            pygit2.clone_repository(self.uri, directory, callbacks=Progress(callback),
                    checkout_branch=self.branch)
        except:
            raise ProjectNotFoundSystemException(self.name)

    def update(self, askmerge, callback=None):
        rename(self.basedir + "/current", self.basedir + "/old")
        self._clone(self.basedir + "/current", callback)
        oldfiles = self._updateFilesFromDirectory(self.basedir + "/old")
        self._updateFiles()
        newfiles = Project.translationfiles
        for mfile in newfiles:
            path = mfile['filename']
            newformat = mfile['format']
            oldformat = None
            for mmfile in oldfiles:
                if mmfile['filename'] == path:
                    oldformat = mmfile['format']
            if oldformat is None:
                continue
            newformat.merge(oldformat, askmerge)
        rmdir(self.basedir + "/old")

    def send(self, interface):
        raise Exception("Unimplemented method in concrete class: send")

    def save(self):
        for resource in Project.translationfiles:
            resource['format'].save()

    def content(self):
        content = {}
        for resource in Project.translationfiles:
            content[resource['filename']] = resource['format'].content()
        return content

    def getExternalFiles(self):
        return [x['format'].getExternalFiles() for x in Project.translationfiles]
    
    def reload(self):
        for x in Project.translationfiles:
            x['format'].reload()

    def getSystemConfigSpec(data=None):
        return []
