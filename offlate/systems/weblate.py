#   Copyright (c) 2021 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
""" The weblate system connector. """

import json
import os
import requests
from requests.auth import HTTPBasicAuth
from pathlib import Path
from zipfile import ZipFile

from ..core.config import *
from ..formats.androidstrings import AndroidStringsFormat
from ..formats.appstore import AppstoreFormat
from ..formats.gettext import GettextFormat
from ..formats.ts import TSFormat
from ..formats.exception import UnsupportedFormatException
from .exception import ProjectNotFoundSystemException
from .project import Project

class WeblateProject(Project):
    def __init__(self, name, lang, conf, data={}):
        Project.__init__(self, name, lang, conf, data)
        for server in self.conf['servers']:
            if server['server'] == data['instance']:
                self.conf['token'] = server['token']
        self.basedir = ''
        self.contents = {}

    def open(self, basedir):
        self.basedir = basedir
        with open(self.basedir + '/project.info') as f:
            self.files = json.load(f)

    def initialize(self, basedir, callback=None):
        self.basedir = basedir
        self.updateFileList()
        with open(self.basedir + '/project.info', 'w') as f:
            f.write(json.dumps(self.files))
        i = 0
        for slug in self.files:
            callback.reportProgress(100.0 * i / len(self.files))
            self.getFiles(slug['slug'])
            i += 1
        callback.reportProgress(100)

    def updateFileList(self):
        self.files = []
        ans = requests.get(self.data['instance'] + '/api/projects/' +
                self.data['project'] + '/components/',
                auth=HTTPBasicAuth('Token', self.conf['token']))
        if ans.status_code == 200:
            l = list(filter(lambda x: x['is_glossary'] == False, json.loads(ans.text)['results']))
            self.files = l
        else:
            print(ans.text)
            raise ProjectNotFoundSystemException(self.name)

    def update(self, askmerge, callback=None):
        self.updateFileList()
        i = 0
        for ff in self.files:
            if callback is not None:
                callback.reportProgress(100.0 * i / len(self.files))
            slug = ff['slug']
            fname = self.filename(slug, False)
            sname = self.filename(slug, True)
            os.rename(fname, fname+'.old')
            os.rename(sname, sname+'.old')
            self.getFiles(slug)
            if ff['file_format'] == 'po':
                oldformat = GettextFormat({'file': fname + '.old',
                    'pot': sname + '.old',
                    'version': self.conf['offlate_version'],
                    'fullname': self.conf['name'] + ' <' + self.conf['email'] + '>',
                    'lang': self.lang})
                currentformat = GettextFormat({'file': fname,
                    'pot': sname,
                    'version': self.conf['offlate_version'],
                    'fullname': self.conf['name'] + ' <' + self.conf['email'] + '>',
                    'lang': self.lang})
            elif ff['file_format'] == 'ts':
                oldformat = TSFormat({'file': fname + '.old', 'lang': self.lang,
                    'template': sname + '.old'})
                currentformat = TSFormat({'file': fname, 'lang': self.lang,
                    'template': sname})
            elif ff['file_format'] == 'aresource':
                oldformat = AndroidStringsFormat({'file': fname + '.old',
                    'lang': self.lang,
                    'template': sname + '.old'})
                currentformat = AndroidStringsFormat({'file': fname,
                    'lang': self.lang,
                    'template': sname})
            elif ff['file_format'] == 'appstore':
                oldformat = AppstoreFormat({'file': fname + '.old',
                    'lang': self.lang,
                    'template': sname + '.old'})
                currentformat = AppstoreFormat({'file': fname,
                    'lang': self.lang,
                    'template': sname})
            else:
                raise UnsupportedFormatException(ff['file_format'])
            currentformat.merge(oldformat, askmerge)
            i += 1
        if callback is not None:
            callback.reportProgress(100)

    def filename(self, slug, is_source):
        ext = ''
        f = None
        for ff in self.files:
            if ff['slug'] == slug:
                f = ff
                break
        if f['file_format'] == 'po':
            ext = 'po'
        elif f['file_format'] == 'ts':
            ext = 'ts'
        elif f['file_format'] == 'aresource':
            ext = 'xml'
        elif f['file_format'] == 'appstore':
            ext = 'txt'
        else:
            raise UnsupportedFormatException(ff['file_format'])
        return self.basedir + '/' + slug + ('.source' if is_source else '') + '.' + ext

    def getFiles(self, slug):
        ans = requests.get(self.data['instance'] + '/api/components/' +
                self.data['project'] + '/' + slug + '/',
                auth=HTTPBasicAuth('Token', self.conf['token']))
        source_lang='en'
        if ans.status_code == 200:
            l = json.loads(ans.text)
            source_lang = l['source_language']['code']

        ans = requests.get(self.data['instance'] + '/api/translations/' +
                self.data['project'] + '/' + slug + '/' + self.lang + '/file/',
                auth=HTTPBasicAuth('Token', self.conf['token']))
        mime = ans.headers['content-type']
        if ans.status_code == 200:
            if mime == 'application/zip':
                directory = self.filename(slug, False)
                zipfile = directory + '.zip'
                with open(zipfile, 'wb') as f:
                    f.write(ans.content)
                Path(directory).mkdir(parents=True, exist_ok=True)
                with ZipFile(zipfile, 'r') as myzip:
                    myzip.extractall(path = directory)
            else:
                with open(self.filename(slug, False), 'wb') as f:
                    f.write(ans.content)

        ans = requests.get(self.data['instance'] + '/api/translations/' +
                self.data['project'] + '/' + slug + '/' + source_lang + '/file/',
                auth=HTTPBasicAuth('Token', self.conf['token']))
        mime = ans.headers['content-type']
        if ans.status_code == 200:
            if mime == 'application/zip':
                directory = self.filename(slug, True)
                zipfile = directory + '.zip'
                with open(zipfile, 'wb') as f:
                    f.write(ans.content)
                Path(directory).mkdir(parents=True, exist_ok=True)
                with ZipFile(zipfile, 'r') as myzip:
                    myzip.extractall(path = directory)
            else:
                with open(self.filename(slug, True), 'wb') as f:
                    f.write(ans.content)
        else:
            print(ans.text)

    def send(self, callback):
        self.save()
        i = 0
        for slug in self.files:
            callback.reportProgress(100.0 * i / len(self.files))
            print('{} => {}'.format(slug['slug'], slug['file_format']))
            filename = self.filename(slug['slug'], False)

            # First, check the file has some translations
            has_translations = False
            for c in self.content()[slug['slug']]:
                if c.isTranslated() or c.isFuzzy():
                    has_translations = True
                    break

            if not has_translations:
                continue

            # First check the component exists, and create it otherwise
            ans = requests.get(
                self.data['instance'] + '/api/translations/' +
                self.data['project'] + '/' + slug['slug'] + '/' +
                self.lang + '/')
            if ans.status_code == 404:
                ans = request.post(
                    self.data['instance'] + '/api/components/' +
                    self.data['project'] + '/' + slug['slug'] + '/translations/',
                    data = {'language_code': self.lang},
                    headers={"user-agent": "offlate", "Accept": "application/json",
                        "Authorization": 'Token '+self.conf['token']})
            
            if ans.status_code == 200: 
                # If this file is a ZIP file, we need to repack it first
                zipfile = Path(filename + '.zip')
                if zipfile.exists():
                    zipfile.unlink()
                    with ZipFile(zipfile, 'w') as myzip:
                        myzip.write(filename, arcname="")
                    filename = zipfile

                ans = requests.post(
                    self.data['instance'] + '/api/translations/' +
                        self.data['project'] + '/' + slug['slug'] + '/' +
                        self.lang + '/file/',
                        files={'file': (os.path.basename(filename), open(filename, 'rb'))},
                        headers={"user-agent": "offlate", "Accept": "application/json",
                            "Authorization": 'Token '+self.conf['token']},
                        data={'conflicts': 'replace-translated',
                            'method': 'translate'})
            print(ans)
            print(ans.text)
            i += 1
        callback.reportProgress(100)

    def save(self):
        for slug in self.slugs:
            slug.save()

    def content(self):
        content = {}
        self.slugs = []
        for slug in self.files:
            fname = self.filename(slug['slug'], False)
            sname = self.filename(slug['slug'], True)
            if slug['file_format'] == 'po':
                myslug = GettextFormat({'file': fname,
                    'pot': sname,
                    'version': self.conf['offlate_version'],
                    'fullname': self.conf['name'] + ' <' + self.conf['email'] + '>',
                    'lang': self.lang})
            elif slug['file_format'] == 'ts':
                myslug = TSFormat({'file': fname, 'lang': self.lang,
                    'template': sname})
            elif slug['file_format'] == 'aresource':
                myslug = AndroidStringsFormat({'file': fname,
                    'lang': self.lang,
                    'template': sname})
            elif slug['file_format'] == 'appstore':
                myslug = AppstoreFormat({'file': fname,
                    'lang': self.lang,
                    'template': sname})
            else:
                raise UnsupportedFormatException(ff['file_format'])

            self.slugs.append(myslug)
            content[slug['slug']] = myslug.content()
        return content

    def getExternalFiles(self):
        return [x.getExternalFiles() for x in self.slugs]

    def reload(self):
        for x in self.slugs:
            x.reload()

    @staticmethod
    def getProjectConfigSpec():
        return [StringConfigSpec('instance', Project.tr('Instance'),
            Project.tr('URL of the Weblate instance'),
            Project.tr('https://hosted.weblate.org')),
                StringConfigSpec('project', Project.tr('Project'),
                    Project.tr('Name of the project on the instance'),
                    Project.tr('foo'))]

    @staticmethod
    def getSystemConfigSpec(data=None):
        if data is not None:
            return [RowConfigSpec('servers', Project.tr('Weblate instance configuration'),
                    Project.tr('You need to configure each Weblate instance separately, \
and you haven\'t configured the instance at {} yet.').format(data['instance']),
                    [
                        StringConfigSpec('token', Project.tr('Token'),
                            Project.tr('The token you created from your account. \
You can create it from <a href="#">the API access tab</a> in your account settings.'),
                            link = "{}/accounts/profile/#api".format(data['instance']),
                            placeholder = Project.tr('fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM'))
                    ], 'server', data['instance'])]
        else:
            return [ListConfigSpec('servers', Project.tr('Configured Weblate instances'),
                    Project.tr('You need to find a token for each Weblate instance \
you have an account on. You can create a token by logging into your account, \
going to your settings and in the API Access page.'),
                    [
                    StringConfigSpec('server', Project.tr('Server'),
                        Project.tr('Server name'),
                        placeholder = Project.tr('https://weblate.org')),
                    StringConfigSpec('token', Project.tr('Token'),
                        Project.tr('The token you created from your account'),
                        placeholder = Project.tr('fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM'))
                    ],
                    'server',
                    WeblateProject.hasRequiredRow)]
        return specs

    @staticmethod
    def hasRequiredRow(conf, servers):
        """
        Method used by the configuration system: it checks that the configuration
        refers to a server whose configuration is valid.
        """
        server = conf['instance']
        
        for serv in servers:
            if serv["server"] == server:
                return True
        return False
