#   Copyright (c) 2018 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
""" The gitlab system connector. """

from .git import GitProject
from .project import Project
from ..core.config import *

from urllib.parse import urlparse
import gitlab

class GitlabProject(GitProject):
    def __init__(self, conf, name, lang, data = {}):
        GitProject.__init__(self, conf, name, lang, data)

    def _updateURI(self):
        self.uri = self.data['repo']
        self.branch = self.data['branch']

    def send(self, interface):
        server = urlparse(self.uri).hostname
        token = ""
        for serv in self.conf["servers"]:
            if serv["server"] == server:
                token = serv["token"]
                break

        if token == "":
            interface.gitlabTokenNotFoundError(server)
            return

        gl = gitlab.Gitlab("https://"+server, private_token=token)
        gl.auth()

        currentUser = gl.user.username
        projectname = self.uri.split('/')[-1]
        projectfullname = urlparse(self.uri).path[1:]

        originproject = gl.projects.get(projectfullname)
        try:
            project = gl.projects.get(currentUser + "/" + projectname)
        except:
            project = originproject.forks.create({})

        try:
            branch = project.branches.create({'branch': 'translation', 'ref': self.branch})
        except:
            interface.gitlabBranchError('translation')
            return
        actions = []
        translationfiles = []
        for mfile in Project.translationfiles:
            translationfiles.extend(mfile['format'].translationFiles())

        for mfile in translationfiles:
            mfile = mfile[len(self.basedir + '/current/'):]
            try:
                project.files.get(file_path=mfile, ref=self.branch)
                actions.append({'action': 'update',
                    'file_path': mfile,
                    'content': open(self.basedir + '/current/' + mfile).read()})
            except:
                actions.append({'action': 'create',
                    'file_path': mfile,
                    'content': open(self.basedir + '/current/' + mfile).read()})
        if actions == []:
            return
        project.commits.create({
            'branch': 'translation',
            'commit_message': 'Update \'' + self.lang + '\' translation',
            'actions': actions
            })
        project.mergerequests.create({'source_branch': 'translation',
            'target_branch': self.branch, 'target_project_id': originproject.id,
            'title': 'Update \'' + self.lang + '\' translation'})

    @staticmethod
    def getProjectConfigSpec():
        return [StringConfigSpec('repo', Project.tr('Repository'),
                    Project.tr('Full clone URL for the repository'),
                    'https://...'),
                StringConfigSpec('branch', Project.tr('Branch'),
                    Project.tr('Name of the branch to translate'),
                    Project.tr('master'))]

    @staticmethod
    def getSystemConfigSpec(data=None):
        specs = []
        specs.extend(GitProject.getSystemConfigSpec())
        if data is not None:
            instance = urlparse(data['repo']).hostname
            specs.append(
                RowConfigSpec('servers', Project.tr('Gitlab instance configuration'),
                    Project.tr("You need to configure each Gitlab instance \
separately, and you haven't configured the instance at {} yet.".format(instance)),
                    [
                    StringConfigSpec('token', Project.tr('Token'),
                        Project.tr('The token you created from your account. \
You can create it from <a href="#">the Access Tokens tab</a> in your account settings.'),
                        link = '{}/-/profile/personal_access_tokens'.format(instance),
                        placeholder = 'Lynid8y56urst-TdlUs6')
                    ],
                    'server',
                    instance))
        else:
            specs.append(
                ListConfigSpec('servers', Project.tr('Configured Gitlab instances'),
                    Project.tr('You need to create a token for each Gitlab instance \
you have an account on. You can create a token by logging into your account, \
going to your settings and in the Access Token page.'),
                    [
                    StringConfigSpec('server', Project.tr('Server'),
                        Project.tr('Server name'),
                        placeholder = 'gitlab.com'),
                    StringConfigSpec('token', Project.tr('Token'),
                        Project.tr('The token you created from your account'),
                        placeholder = 'Lynid8y56urst-TdlUs6')
                    ],
                    'server',
                    GitlabProject.hasRequiredRow))
        return specs

    @staticmethod
    def hasRequiredRow(conf, servers):
        """
        Method used by the configuration system: it checks that the configuration
        refers to a server whose configuration is valid.
        """
        server = urlparse(conf['repo']).hostname
        
        for serv in servers:
            if serv["server"] == server:
                return True
        return False
