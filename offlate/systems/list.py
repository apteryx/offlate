#   Copyright (c) 2020 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
""" The list of system connectors. """

from .gitlab import GitlabProject
from .github import GithubProject
from .tp import TPProject
from .transifex import TransifexProject
from .weblate import WeblateProject

systems = [
  {'name': 'Translation Project',
   'system': TPProject,
   'key': 'TP'},
  {'name': 'Transifex',
   'system': TransifexProject,
   'key': 'Transifex'},
  {'name': 'Gitlab',
   'system': GitlabProject,
   'key': 'Gitlab'},
  {'name': 'Github',
   'system': GithubProject,
   'key': 'Github'},
  {'name': 'Weblate',
   'system': WeblateProject,
   'key': 'Weblate'}
]
