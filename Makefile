# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = python3 -msphinx
SPHINXPROJ    = transmon
SOURCEDIR     = doc
BUILDDIR      = _build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile fonts

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

upload:
	rm -rf dist
	python3 setup.py sdist
	gpg --detach-sign -a dist/*.tar.gz
	twine upload dist/*

test-upload:
	rm -rf dist
	python3 setup.py sdist
	gpg --detach-sign -a dist/*.tar.gz
	twine upload -r pypitest dist/*

update-data:
	python3 extractdata.py

fonts: offlate/ui/data/whitespace.ttf offlate/ui/data/whitespace-mono.ttf

# prevent sphinx from taking over this target
%.sfd:

%.ttf: %.sfd
	echo 'Open("$<")' > fontforge_whitespace
	echo 'Generate("$@")' >> fontforge_whitespace
	fontforge -lang=ff -script fontforge_whitespace

LANGS=en fr

update-langs:
	for l in $(LANGS); do \
		pylupdate5 offlate/ui/*.py offlate/ui/config/*.py offlate/systems/*.py \
		  -ts offlate/locales/offlate_$${l}.ts ;\
		lrelease offlate/locales/offlate_$${l}.ts ;\
	done

create-bundle-linux: update-langs
	pyinstaller run.py --add-data offlate/ui/data:offlate/ui/data \
	  --add-data offlate/data:offlate/data \
	  --add-data offlate/locales:offlate/locales \
	  --windowed -n offlate
	patchelf --set-rpath . dist/run/run
	cd dist/run; for f in *.so *.so.*; do patchelf --set-rpath . $$f; done
	mkdir -p dist/bin
	printf "#!/bin/sh\ncd \$$(dirname \$$0)/../run; exec ./run" > dist/bin/offlate
	chmod +x dist/bin/offlate

bundle-linux: create-bundle-linux
	tar cf dist/offlate-bin.tar -C dist/ run bin
	gzip dist/offlate-bin.tar

bundle-windows: update-langs
	pyinstaller run.py --add-data offlate\data;offlate\data \
	  --add-data offlate\ui\data;offlate\ui\data \
	  --add-data offlate\locales;offlate\locales \
	  --windowed \
	  --hidden-import _cffi_backend
